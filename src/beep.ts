export function makeBeep(context: AudioContext) {
  const oscillator = context.createOscillator();
  const g = context.createGain();
  oscillator.connect(g);
  g.connect(context.destination);
  oscillator.start(0);
  oscillator.frequency.value = 1109;
  g.gain.exponentialRampToValueAtTime(0.00001, context.currentTime + 0.4);
  oscillator.stop(4000);
}
