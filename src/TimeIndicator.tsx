import styles from "./App.module.css";

interface Props {
  remainingTime?: number | undefined;
}

function TimeIndicator(props: Props) {
  const { remainingTime } = props;
  return (
    <span className={styles.remainingTime}>
      {remainingTime ? `${remainingTime}` : "0"}
    </span>
  );
}

export default TimeIndicator;
