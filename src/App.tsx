import React, { useState } from "react";

import "normalize.css";
import { CountdownCircleTimer } from "react-countdown-circle-timer";
import styles from "./App.module.css";
import { DRILL_1 } from "./exercises";
import TimeIndicator from "./TimeIndicator";
import { makeBeep } from "./beep";

function App() {
  const [isPlaying, setPlaying] = useState(false);
  const [exerciseIndex, setExerciseIndex] = useState(0);
  const context = new AudioContext();

  if (!DRILL_1[exerciseIndex]) {
    return (
      <div className={styles.app}>
        <div className={styles.step}>Finished !</div>
        <button
          className={styles.playButton}
          onClick={() => setExerciseIndex(0)}
        >
          Recommencer ?
        </button>
      </div>
    );
  }

  const [exercise, duration] = DRILL_1[exerciseIndex];

  return (
    <div className={styles.app}>
      <CountdownCircleTimer
        key={exerciseIndex}
        isPlaying={isPlaying}
        duration={duration}
        colors="#6e82ba"
        trailStrokeWidth={10}
        onComplete={() => {
          makeBeep(context);
          setTimeout(() => {
            setExerciseIndex(exerciseIndex + 1);
          }, 1000);
        }}
      >
        {(props) => <TimeIndicator {...props} />}
      </CountdownCircleTimer>
      <div className={styles.step}>{exercise}</div>
      <div className={styles.nextStep}>
        {`Next: ${
          DRILL_1[exerciseIndex + 1]
            ? DRILL_1[exerciseIndex + 1][0]
            : "Finished !"
        }`}
      </div>
      <button
        className={styles.playButton}
        onClick={() => setPlaying(!isPlaying)}
      >
        {isPlaying ? "Pause" : "Play"}
      </button>
    </div>
  );
}

export default App;
