export enum Exercise {
  Squats = "Squats",
  Planche = "Planche",
  Avion = "Avion",
  Fentes = "Fentes",
  Pause = "Pause",
}

export const DRILL_1: [Exercise, number][] = [
  [Exercise.Squats, 30],
  [Exercise.Pause, 10],
  [Exercise.Planche, 30],
  [Exercise.Pause, 10],
  [Exercise.Avion, 30],
  [Exercise.Pause, 10],
  [Exercise.Fentes, 30],
  [Exercise.Pause, 10],
  [Exercise.Squats, 30],
  [Exercise.Pause, 10],
  [Exercise.Planche, 30],
  [Exercise.Pause, 10],
  [Exercise.Avion, 30],
  [Exercise.Pause, 10],
  [Exercise.Fentes, 30],
];
